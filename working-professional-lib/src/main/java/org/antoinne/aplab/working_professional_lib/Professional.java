package org.antoinne.aplab.working_professional_lib;

import java.util.ArrayList;

public abstract class Professional {
	
	private int id;
	private String name;
	private float salary;
	private ArrayList<Certification> certifications = new ArrayList<Certification>(5);
	
	public void Professional (String name) {
	}
	
	public void addCertification(Certification cert) {	
	}

	public Professional(int id, String name, float salary, ArrayList<Certification> certifications) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.certifications = certifications;
	}

	public Professional(Router ro, Tool tool) {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public ArrayList<Certification> getCertifications() {
		return certifications;
	}

	public void setCertifications(ArrayList<Certification> certifications) {
		this.certifications = certifications;
	}
	
	
}
