package org.antoinne.aplab.working_professional_lib;

public interface IPlumbingProfessional {
	public Tool tool;
	private Pipe pipe;
	
	public boolean fixPipe(Pipe pip, Tool tool);
	
}
