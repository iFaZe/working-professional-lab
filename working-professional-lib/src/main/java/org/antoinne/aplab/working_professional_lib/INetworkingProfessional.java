package org.antoinne.aplab.working_professional_lib;

public interface INetworkingProfessional {
	
	public Router ro;
	private Tool tool;
	

	public void Tool(Tool tool);
	
	public void Router(Router ro);
	
	public boolean fixRouter(Router ro, Tool tool);
	
	
}
