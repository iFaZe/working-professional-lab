package org.antoinne.aplab.working_professional_lib;

public abstract class Tool {
	
	private String brand;
	
	public void Tool(String str) {
	}
	
	public boolean fix(BreakableItem brea) {
		return false;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Tool(String brand) {
		super();
		this.brand = brand;
	}
	
	
}
