package org.antoinne.aplab.working_professional_lib;

public final class CiscoRouter extends Router{
	
	private int CiscoCode;

	public int getCiscoCode() {
		return CiscoCode;
	}

	public void setCiscoCode(int ciscoCode) {
		CiscoCode = ciscoCode;
	}

	public CiscoRouter(String brand, float bandwidth, int ciscoCode) {
		super(brand, bandwidth);
		CiscoCode = ciscoCode;
	}
	
	
}
