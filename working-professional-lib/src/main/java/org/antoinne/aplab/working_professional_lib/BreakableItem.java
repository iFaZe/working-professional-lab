package org.antoinne.aplab.working_professional_lib;

public interface BreakableItem {

	public void updateDamage(float damage);
}
