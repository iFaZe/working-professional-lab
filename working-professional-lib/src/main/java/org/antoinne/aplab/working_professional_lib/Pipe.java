package org.antoinne.aplab.working_professional_lib;

public abstract class Pipe implements BreakableItem {
	
	protected float length;
	protected float diameter;
	
	public Pipe(float length, float diameter) {
		super();
		this.length = length;
		this.diameter = diameter;
	}

	public void updateDamage(float damage) {	
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public float getDiameter() {
		return diameter;
	}

	public void setDiameter(float diameter) {
		this.diameter = diameter;
	}
	
	
}
