package org.antoinne.aplab.working_professional_lib;

public class PvcPipe extends Pipe {
	
	private String colour;

	public PvcPipe(float length, float diameter, String colour) {
		super(length, diameter);
		this.colour = colour;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}
	
}
