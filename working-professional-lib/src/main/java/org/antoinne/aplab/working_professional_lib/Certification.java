package org.antoinne.aplab.working_professional_lib;

public class Certification {
	
	private int number;
	private String name;
	private Professional professional;
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Professional getProfessional() {
		return professional;
	}
	public void setProfessional(Professional professional) {
		this.professional = professional;
	}
	public Certification(int number, String name, Professional professional) {
		super();
		this.number = number;
		this.name = name;
		this.professional = professional;
	}
	
	
}

